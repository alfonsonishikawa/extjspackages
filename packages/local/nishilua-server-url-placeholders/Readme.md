# nishilua-server-url-placeholders - Read Me

* 2017-12-11: Compatibility with ExtJS 6.2.0.981
* 2017-09-27: Initial version allowing placeholders. Modified Rest Proxy to never send id as query param when `appendId === false`