nishilua-escaped-binds - Read Me
================================

Examples
--------

* ExtJS 6.2.0.981 - https://fiddle.sencha.com/#view/editor&fiddle/2amc
* ExtJS 6.0.1.250 - https://fiddle.sencha.com/#view/editor&fiddle/1hgt

Changes
-------

* 2017.12.13: Bugfixes 6.2.0.981 not accepting negated binds generic unescaped letters. Tokenizer improvement.
* 2017.12.09: Added support for ExtJS 6.2.0.981 
* 2017.11.30: Added support: binding to array items via index. https://www.sencha.com/forum/showthread.php?291446-Binding-to-array-item-via-index
* 2017.08.09: Bugfix: When using a Formatter in a expression it was not being detected as a Template
* 2017.04.10: Fixed Ext.String not found (https://www.sencha.com/forum/showthread.php?285874-Cannot-override-Ext-String-(Sencha-cmd)&p=1177787&viewfull=1#post1177787)
* 2017.02.14: Changed String override because of error 'C2016: Override target not found'.
* 2016.09.28: Bugfix: added '!' to escaped characters set (. \ { } ! :}. Added example application.
* 2016.09.27: Regexp tunning, especially in Nishilua.app.ViewModel.
* 2016.09.26: Initial version
