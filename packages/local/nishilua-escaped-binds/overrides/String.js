/*
Escaped-binds package is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License 3.0 as published by
the Free Software Foundation.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

 http://www.gnu.org/licenses/gpl.html
*/
/**
 * Override that adds three methods to use with escaped bind expressions: escapeBind, unescapeBind and
 * splitByUnescapedDots
 *
 * Implementation based on https://www.sencha.com/forum/showthread.php?285874-Cannot-override-Ext-String-(Sencha-cmd)
 *
 */
Ext.define('Nishilua.String', {
    requires : [
        'Ext.String'
    ],
    compatibility: [
        '6.0.1.250',
        '6.2.0.981'
    ]

}, function() {

    delete Nishilua.String;

    /**
     * Regular expression that matches the characters to escape: '.', '\', '{', '}', '!', ':', ' ' and '('
     */
    var escapeBindRe = /(\.|\\|\{|}|!|:| |\()/g ;

    /**
     * Regular expression that matches the characters to unescape: '\.', '\\', '\{', '\}', '\!', '\:', ' ' and '('
     */
    var unescapeBindRe = /(\\([.\\{}!: (]))/g ;

    /**
     * Given a String, escapes it to become a single bind token.
     * The characters ".", "\", "{" and "}" are escaped with "\"
     *
     * For example:
     *
     * 'token.another' -> 'token\.another'
     *
     * To scape only a subexpression, it must be concatenated:
     *
     * ```
     * 'token.' + escapeBind('another.andanother') -> 'token.another\.andanother'
     * ```
     *
     * @param {String} string - The text to escape
     * @returns {String} The scaped string
     */
    Ext.String.escapeBind = function(string) {
        return string.replace(escapeBindRe, '\\$1');
    } ;

    /**
     * Given a String escaped with {@link #escapeBind}, unescapes it.
     *
     * For example:
     *
     * 'token\.another' -> 'token.another'
     *
     * @param {String} string - The text to escape
     * @returns {String} The scaped string
     */
    Ext.String.unescapeBind = function(string) {
        return string.replace(unescapeBindRe, '$2');
    } ;

    /**
     * Splits a string of dot separated tokens into an array. If the
     * tokens are already an array, it is returned.
     *
     * Can not handle negated expressions ("!foo.bar")
     *
     * Based on split solution by ridgerunner from {@link http://stackoverflow.com/a/12922554/582789}
     *
     * @param {String/Array} string
     * @returns {Array}
     */
    Ext.String.splitByUnescapedDots = function(string) {
        if (string && typeof string === 'string') {
            var tokens = [];
            if (string === '') return tokens;
            // The first token starts from beginning of the string: ^
            string = string.replace(/^[^.\\]*(?:\\[\S\s][^.\\]*)*(?=\.|$)/,
                function(token){tokens.push(token); return ''/*delete the token inside the string*/;});
            // The rest of the tokens starts from a dot
            string = string.replace(/\.([^.\\]*(?:\\[\S\s][^.\\]*)*)/g,
                function(_, token){tokens.push(token); return ''/*delete the token inside the string*/;});
            return tokens;
        }
        return string || [];
    } ;

    /**
     * Searches for an unescaped character in a string
     * @param {String} text - The string to search into for the unescaped character
     * @param {String} character - The character to search (in the text must appear unescaped)
     * @param {Number} [startPos=0] - The start position to search
     * @returns the position of the unescaped character or -1 if not found
         */
    Ext.String.findUnescapedCharacer = function (text, character, startPos) {
        var endPos = -1,
            length = text.length,
            escaped = false,
            startPos = startPos || 0,
            c;

        for (var currentPos = startPos; currentPos < length; currentPos++) {
            if (escaped === true) {
                escaped = false;
                continue;
            }
            c = text.charAt(currentPos) ;
            if (c === '\\') {
                escaped = true ;
                continue;
            }
            if (c === character) {
                endPos = currentPos ;
                break ;
            }
        }
        return endPos ;
    } ;

});
