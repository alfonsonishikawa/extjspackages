/**
 * This class is used to parse a string into a series of tokens. The syntax of the string
 * is JavaScript-like. This class is useful for creating higher-level parsers to allow
 * them to assemble tokens into a meaningful language (such as bind properties).
 *
 * The following set of punctuation characters are supported:
 *
 *      + - * / ! , : [ ] { } ( )
 *
 * This class does not currently separate the dot operator but instead includes it in a
 * single "ident" token. Whitespace between tokens is skipped.
 *
 * Tokens are parsed on-demand when `next` or `peek` are called. As much as possible,
 * the returned tokens are reused (e.g., to represent tokens like ":" the same object is
 * always returned). For tokens that contain values, a new object must be created to
 * return the value. Even so, the `is` property that describes the data is a reused object
 * in all cases.
 *
 *      var tokenizer;  // see below for getting instance
 *
 *      for (;;) {
 *          if (!(token = tokenizer.next())) {
 *              // When null is returned, there are no more tokens
 *
 *              break;
 *          }
 *
 *          var is = token.is;  // the token's classification object
 *
 *          if (is.error) {
 *              // Once an error is encountered, it will always be returned by
 *              // peek or next. The error is cleared by calling reset().
 *
 *              console.log('Syntax error', token.message);
 *              break;
 *          }
 *
 *          if (is.ident) {
 *              // an identifier...
 *              // use token.value to access the name or dot-path
 *
 *              var t = tokenizer.peek();  // don't consume next token (yet)
 *
 *              if (t && t.is.parenOpen) {
 *                  tokenizer.next();  // we'll take this one
 *
 *                  parseThingsInParens();
 *
 *                  t = tokenizer.next();
 *
 *                  mustBeCloseParen(t);
 *              }
 *          }
 *          else if (is.literal) {
 *              // a literal value (null, true/false, string, number)
 *              // use token.value to access the value
 *          }
 *          else if (is.at) {
 *              // @
 *          }
 *      }
 *
 * For details on the returned token see the `peek` method.
 *
 * There is a pool of flyweight instances to reduce memory allocation.
 *
 *      var tokenizer = Ext.parse.Tokenizer.fly('some.thing:foo()');
 *
 *      // use tokenizer (see above)
 *
 *      tokenizer.release();  // returns the fly to the flyweigt pool
 *
 * The `release` method returns the flyweight to the pool for later reuse. Failure to call
 * `release` will leave the flyweight empty which simply forces the `fly` method to always
 * create new instances on each call.
 *
 * A tokenizer can also be reused by calling its `reset` method and giving it new text to
 * tokenize.
 *
 *      this.tokenizer = new Ext.parse.Tokenizer();
 *
 *      // Later...
 *
 *      this.tokenizer.reset('some.thing:foo()');
 *
 *      // use tokenizer (see above)
 *
 *      this.tokenizer.reset();
 *
 * The final call to `reset` is optional but will avoid holding large text strings or
 * parsed results that rae no longer needed.
 *
 * @private
 */
Ext.define('Nishilua.parse.Tokenizer62', {
    override: 'Ext.parse.Tokenizer',

    compatibility: '6.2.0.981',

    privates: {

        /**
         * Regular expression that matches the first possible letter of an identifier.
         * Must not be  '!', '.', ':', ' ', '}' nor '(' <-- in all this cases should be escaped.
         */
        identFirstRe: /[^!.: }(]/i,

        /**
         * When parsing an 'ident', the identifier is scanned letter by letter (acceting escaped characters).
         * Regular expression that matches each letter of an identifier (the parser already jumps over escaped characters).
         * Must not be  '!', '.', ':', ' ', '}' nor '(' <-- in all this cases should be escaped.
         * - 'exclamation' is the negation of an identifier
         * - Dot splits identifiers
         * - Colons slits an identifier from the formatter
         * - Espaces could split expressions
         * - Closing braces mark the end of a bind expression
         * - Opening parentheses mark the en of a formatter name
         */
        identRe: /[^!.: }(]/i,

        /**
         * Parses the current token that starts with the provided character `c` and
         * located at the current `pos` in the `text`.
         * @param {String} c The current character.
         * @return {Object} The next token
         */
        parse: function (c) {
            var me = this,
                digitRe = me.digitRe,
                text = me.text,
                length = me.end,
                ret;

            // Handle ".123"
            if ( c === '.' && me.pos+1 < length) {
                if (digitRe.test(text.charAt(me.pos+1))) {
                    ret = me.parseNumber();
                }
            }

            if (!ret && me.operators[c]) {
                ret = me.parseOperator(c);
            }

            if (!ret) {
                if (c === '"' || c === "'") {
                    ret = me.parseString();
                }
                else if (digitRe.test(c)) {
                    ret = me.parseNumber();
                }
                else if (me.identFirstRe.test(c)) {
                    ret = me.parseIdent();
                }
                else {
                    ret = me.syntaxError('Unexpected character');
                }
            }

            return ret;
        },

        /**
         * Parses the next identifier token.
         * @return {Object} The next token.
         */
        parseIdent: function () {
            var me = this,
                identRe = me.identRe,
                keywords = me.getKeywords(),
                includeDots = !me.operators['.'],
                text = me.text,
                start = me.pos,
                end = start,
                length = me.end,
                prev = 0,
                c, value;

            while (end < length) {
                c = text.charAt(end);

                if (includeDots && c === '.') {
                    if (prev === '.') {
                        return me.syntaxError(end, 'Unexpected dot operator');
                    }
                    ++end;
                }
                else if (identRe.test(c)) {
                    ++end;
                }
                else if (c === '\\') { // +2
                    ++end ;
                    if (end < length) {
                        ++end ;
                    }
                }
                else {
                    break;
                }

                prev = c;
            }

            if (prev === '.') {
                return me.syntaxError(end - 1, 'Unexpected dot operator');
            }

            value = text.substring(start, me.pos = end);

            return (keywords && keywords[value]) || {
                type: 'ident',
                is: { ident: true },
                value: value
            };
        }

    }
});
