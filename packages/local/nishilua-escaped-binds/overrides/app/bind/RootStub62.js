/*
 Escaped-binds package is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License 3.0 as published by
 the Free Software Foundation.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 http://www.gnu.org/licenses/gpl.html
 */
/**
 * Override regarding to unescaping keys when accessing values.
 * The tokens flow escaped until the moment it is needed to check that exists as a key.
 */
Ext.define('Nishilua.app.bind.RootStub62', {
    override: 'Ext.app.bind.RootStub',

    compatibility: '6.2.0.981',

    set: function (value) {
        //<debug>
        if (!value || value.constructor !== Object) {
            Ext.raise('Only an object can be set at the root');
        }
        //</debug>

        var me = this,
            children = me.children || (me.children = {}),
            owner = me.owner,
            data = owner.data,
            parentVM = owner.getParent(),
            linkStub, stub, v, key;

        for (key in value) {
            // Commented out a debug guard that not allowed names with dots

            // Setting the value.
            // Ensure the Stub exists for the name, and set its value.
            if ((v = value[key]) !== undefined) {
                if (!(stub = children[key])) {
                    stub = new Ext.app.bind.Stub(owner, key, me);
                } else if (stub.isLinkStub) {
                    if (!stub.getLinkFormulaStub()) {
                        // Pass parent=null since we will graft in this new stub to replace us:
                        linkStub = stub;
                        stub = new Ext.app.bind.Stub(owner, key);
                        linkStub.graft(stub);
                    }
                }

                stub.set(v);
            }
            // Clearing the value. Delete the data item
            // Invalidate the Stub if it exists.
            else if (data.hasOwnProperty(key)) {
                delete data[key];

                stub = children[key];
                if (stub) {
                    if (!stub.isLinkStub && parentVM) {
                        stub = me.createRootChild(key);
                    }
                    stub.invalidate(true);
                }
            }
        }
    },

    descend: function (path, index) {
        var me = this,
            children = me.children,
            pos = index || 0,
            name = Ext.String.unescapeBind(path[pos++]),
            ret = (children && children[name]) || me.createRootChild(name);

        if (pos < path.length) {
            ret = ret.descend(path, pos);
        }

        return ret;
    }

});