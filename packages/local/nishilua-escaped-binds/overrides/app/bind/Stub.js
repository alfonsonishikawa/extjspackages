/*
 Escaped-binds package is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License 3.0 as published by
 the Free Software Foundation.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 http://www.gnu.org/licenses/gpl.html
 */
/**
 * Override regarding to escaping bind expressions when generating the path from an unescaped expression, and
 * unescaping keys when accessing values.
 * The tokens flow escaped until the moment it is needed to check that exists as a key.
 *
 * Modifications made to allow binding to array items via index (the code was missing `.constructor === Array` checks).
 */
Ext.define('Nishilua.app.bind.Stub', {
    override: 'Ext.app.bind.Stub',

    compatibility: '6.0.1.250',

    statics: {
        trackHadValue: function(value, owner, path, stub) {
            var children = stub && stub.children,
                child, key, hadValue;

            // Keep track of the fact that we've had a value set. We may get set
            // to undefined in the future, we only need to know whether we
            // are initially in an undefined state
            hadValue = value !== undefined;
            if (!owner.hadValue[path]) {
                owner.hadValue[path] = hadValue;
            }

            if (stub) {
                stub.hadValue = hadValue;
            }

            if (value && (value.constructor === Object || value.constructor === Array || value.isModel)) {
                if (value.isModel) {
                    value = value.data;
                }

                for (key in value) {
                    Ext.app.bind.Stub.trackHadValue(value[key], owner, path + '.' + Ext.String.escapeBind(key), children && children[key]);
                }
            }
        }
    },

    descend: function (path, index) {
        var me = this,
            children = me.children || (me.children = {}),
            pos = index || 0,
            name = Ext.String.unescapeBind(path[pos++]),
            ret;

        if (!(ret = children[name])) {
            ret = new Ext.app.bind.Stub(me.owner, name, me);
        }

        if (pos < path.length) {
            ret = ret.descend(path, pos);
        }

        return ret;
    },

    getDataObject: function () {
        var me = this,
            parentData = me.parent.getDataObject(), // RootStub does not get here
            name = me.name,
            ret = parentData ? parentData[name] : null,
            associations, association;

        if (!ret && parentData && parentData.isEntity) {
            // Check if the item is an association, if it is, grab it but don't load it.
            associations = parentData.associations;
            if (associations && name in associations) {
                ret = parentData[associations[name].getterName]();
            }
        }

        if (!ret || !(ret.$className || Ext.isObject(ret) || Ext.isArray(ret))) {
            parentData[name] = ret = {};
            // We're implicitly setting a value on the object here
            me.hadValue = me.owner.hadValue[me.path] = true;
            // If we're creating the parent data object, invalidate the dirty
            // flag on our children.
            me.invalidate(true, true);
        }

        return ret;
    },

    set: function (value) {
        var me = this,
            parent = me.parent,
            name = me.name,
            formula = me.formula,
            parentData, associations, association, formulaStub;

        if (formula && !formula.settingValue && formula.set) {
            formula.setValue(value);
            return;
        } else if (me.isLinkStub) {
            formulaStub = me.getLinkFormulaStub();
            formula = formulaStub ? formulaStub.formula : null;
            if (formula) {
                //<debug>
                if (formulaStub.isReadOnly()) {
                    Ext.raise('Cannot setValue on a readonly formula');
                }
                //</debug>
                formula.setValue(value);
                return;
            }
        }

        // To set a child property, the parent must be an object...
        parentData = parent.getDataObject();

        if (parentData.isEntity) {
            associations = parentData.associations;

            if (associations && (name in associations)) {
                association = associations[name];
                parentData[association.setterName](value);
                // We may be setting a record here, force the value to recalculate
                me.invalidate(true);
            } else {
                // If not an association then it is a data field
                parentData.set(name, value);
            }

            // Setting fields or associated records will fire change notifications so we
            // handle the side effects there
        } else if ((value && (value.constructor === Object || value.constructor === Array)) || value !== parentData[name]) {
            if (!me.setByLink(value)) {
                if (value === undefined) {
                    delete parentData[name];
                } else {
                    parentData[name] = value;
                    Ext.app.bind.Stub.trackHadValue(value, me.owner, me.path, me);
                }

                me.inspectValue(parentData);
                // We have children, but we're overwriting the value with something else, so
                // we need to schedule our children
                me.invalidate(true);
            }
        }
    }

});