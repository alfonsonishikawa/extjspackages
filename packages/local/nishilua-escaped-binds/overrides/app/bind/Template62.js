/*
 Escaped-binds package is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License 3.0 as published by
 the Free Software Foundation.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 http://www.gnu.org/licenses/gpl.html
 */
/**
 * Overrides the parse() method of Ext.app.bind.Template' improving the matching function to match escaped tokens.
 *
 * Sadly, the featured original regular expression to match "{!foo:this.fmt(2,4)}" had to be implemented with a
 * set of string scanning private methods. Fortunately, the test runs in O(n) where 'n' is the string length.
 *
 * Any good regular expression will be welcome.
 *
 */
Ext.define('Nishilua.app.bind.Template62', {
    override: 'Ext.app.bind.Template',

    compatibility: '6.2.0.981',

    privates: {
        /**
         * Parses the template text into `buffer`, `slots` and `tokens`. This method is called
         * automatically when the template is first used.
         * @return {Ext.app.bind.Template} this
         * @private
         */
        parse: function () {
            // NOTE: The particulars of what is stored here, while private, are likely to be
            // important to Sencha Architect so changes need to be coordinated.
            var me = this,
                text = me.text,
                parser = Ext.app.bind.Parser.fly(),
                buffer = (me.buffer = []),
                slots = (me.slots = []),
                last = 0,
                length = text.length,
                pos = 0,
                i;

            // Remove the initters so that we don't get called here again.
            for (i in me._initters) {
                delete me[i];
            }

            me.tokens = [];
            me.tokensMap = {};

            // text = 'Hello {foo:this.fmt(2,4)} World {bar} - {1}'
            for (i = 0; i < length;) {
                last = i;
                if ((i = Ext.String.findUnescapedCharacer(text, '{', last)) < 0) {
                    buffer[pos++] = text.substring(last);
                    break;
                }

                if (last < i) {
                    buffer[pos++] = text.substring(last, i);
                }

                parser.reset(text, i + 1);
                slots[pos++] = parser.compileExpression(me.tokens, me.tokensMap);

                i = parser.token.at + 1;  // skip over the "}" token
                parser.expect('}');      // ensure the next token is "}"
            }

            parser.release();

            me.single = buffer.length === 0 && slots.length === 1;

            return me;
        }

    }

}) ;