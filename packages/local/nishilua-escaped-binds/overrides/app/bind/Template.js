/*
 Escaped-binds package is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License 3.0 as published by
 the Free Software Foundation.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 http://www.gnu.org/licenses/gpl.html
 */
/**
 * Overrides the parse() method of Ext.app.bind.Template' improving the matching function to match escaped tokens.
 *
 * Sadly, the featured original regular expression to match "{!foo:this.fmt(2,4)}" had to be implemented with a
 * set of string scanning private methods. Fortunately, the test runs in O(n) where 'n' is the string length.
 *
 * Any good regular expression will be welcome.
 *
 */
Ext.define('Nishilua.app.bind.Template', {
    override: 'Ext.app.bind.Template',

    compatibility: '6.0.1.250',

    /**
     * Searches for a match starting (including) from startIndex.
     *
     * @param {String} string
     * @param {Number} startIndex
     *
     * @return {Array/Boolean} - Array/Object with the match info:
     * ```
     * {
     *     0: matched slot
     *     1: index identifier
     *     2: token
     *     3: format
     *     4: args
     *     index: slot index in input
     *     input: original string
     * }
     * ```
     * @protected
     */
    getMatch: function(string, startIndex) {
        var me = this ;

        var slotMatch = me.getSlotMatch(string, startIndex) ;
        if (slotMatch === false)
            return false ;

        var parseIndex = 1 ; // Ignore the starting '{'
        if (slotMatch[0][1] === '!') {
            parseIndex++ ; // Jump over negation of the expression
        }

        if (me.fillIndex(slotMatch, parseIndex)) {
            parseIndex += 2 ; // Index is a 1-character number: "{1:..." "{1}"
                              // jump over the index and ":" or "}"
        } else { // "index" xor "token"
            if (me.fillToken(slotMatch, parseIndex)) {
                parseIndex += slotMatch[2].length + 1 ; // jump over the token and ":" or "}"
            } else {
                Ext.raise('Unfinished token: [ ' + slotMatch[0] + ' ] in template: [ ' + string + ' ]') ;
            }
        }

        if(me.fillFmt(slotMatch, parseIndex)) {
            parseIndex += slotMatch[3].length + 1 ; // jump over the format name and "(" or "}"
            me.fillArgs(slotMatch, parseIndex) ;
        }

        return slotMatch ;
    },

    /**
     * Returns next slot '{...}' or null if there is no more
     * @param {String} string - The complete string
     * @param {Number} startIndex - Inclusive start index to search for the next slot
     *
     * @return {Object} - { index: {Number} beginning of match index, 0: {String} matched slot } | null if no slot found
     * @private
     */
    getSlotMatch: function(string, startIndex) {
        var numBackslashes = 0 ; // If an odd backslashes => character escaped
        var beginIndex = startIndex ;
        var foundSlotBegin = false;
        var foundSlotEnd = false ;

        while ( !foundSlotBegin ) {
            if (beginIndex === string.length)
                return false ; // Finished without finding a beginning
            switch (string[beginIndex]) {
                case '\\':
                    numBackslashes++ ;
                    beginIndex++ ;
                    break ;
                case '{':
                    if (numBackslashes % 2 === 0) {
                        // Even means unescaped {
                        foundSlotBegin = true ;
                    } else {
                        beginIndex++ ;
                    }
                    break ;
                default:
                    numBackslashes = 0 ; // Rest backslashes
                    beginIndex++ ;
            }
        }

        var endIndex = beginIndex + 1 ;

        while ( !foundSlotEnd ) {
            if (endIndex === string.length)
                return null ; // Finished without finding an end
            switch (string[endIndex]) {
                case '\\':
                    numBackslashes++ ;
                    endIndex++ ;
                    break ;
                case '}':
                    if (numBackslashes % 2 === 0) {
                        // Even means unescaped }
                        foundSlotEnd = true ;
                    }
                    endIndex++ ; // We want the ending } too
                    break ;
                default:
                    numBackslashes = 0 ; // Rest backslashes
                    endIndex++ ;
            }
        }

        var result = [string.substring(beginIndex, endIndex)] ;
        result.index = beginIndex ;
        result.input = string ;

        return result ;
    },

    /**
     * Given the slot in slotMatch[0], fill the information of index value in slotMatch[1] if exists.
     * The index is the number value in "{!1}" and "{!1:...}"
     *
     * @param {Array} slotMatch -
     * @param {Number} startIndex - Inclusive start index to search for the index value (if exists) of the slot
     *
     * @returns {Array/Boolean} slotMatch with slotMatch[1] filled, or false if there is no index value in the slot string.
     * @private
     */
    fillIndex: function(slotMatch, parseIndex) {
        if (this.numberRe.test(slotMatch[0][parseIndex])) {
            slotMatch[1] = slotMatch[0][parseIndex] ;
            return slotMatch ;
        }
        return false ;
    },

    /**
     * Given the slot in slotMatch[0], fills the information of token value in slotMatch[2] if exists.
     * The token is the string value "foo" in "{!foo}" and "{!foo:...}"
     *
     * @param {Array} slotMatch -
     * @param {Number} startIndex - Inclusive start index to search for the token (must exists) of slot
     *
     * @returns {Array/Boolean} slotMatch with slotMatch[2] filled, or false if there is no token value in the slot string.
     * @private
     */
    fillToken: function(slotMatch, parseIndex) {
        var numBackslashes = 0 ; // If an odd backslashes => character escaped
        var foundTokenEnd = false ;

        var endIndex = parseIndex ;
        while ( !foundTokenEnd ) {
            if (endIndex === slotMatch[0].length) return false ; // Finished without finding an end
            switch (slotMatch[0][endIndex]) {
                case '\\':
                    numBackslashes++ ;
                    endIndex++ ;
                    break ;
                case ':':
                case '}':
                    if (numBackslashes % 2 === 0) {
                        // Even means unescaped }
                        foundTokenEnd = true ;
                    } else {
                        endIndex++;
                    }
                    break ;
                default:
                    numBackslashes = 0 ; // Rest backslashes
                    endIndex++ ;
            }
        }

        slotMatch[2] = slotMatch[0].substring(parseIndex, endIndex) ;
        return slotMatch;
    },

    /**
     * Given the slot in slotMatch[0], fills the information of format value (if exists) in slotMatch[3].
     * The format is the string value "fmt" in "{!1:fmt(args)}" and "{!foo:fmt(args)}"
     *
     * @param {Array} slotMatch -
     * @param {Number} startIndex - Inclusive start index to search for the format (if exists)
     *
     * @returns {Array/Boolean} slotMatch with slotMatch[3] filled, or false if there is no format string.
     * @private
     */
    fillFmt: function(slotMatch, parseIndex) {
        var endIndex = parseIndex ;

        if (parseIndex >= slotMatch[0].length)
            return false ;

        while( slotMatch[0][endIndex] !== '(' && slotMatch[0][endIndex] !== '}' ) {
            endIndex++ ;
        }
        slotMatch[3] = slotMatch[0].substring(parseIndex, endIndex) ;
        return slotMatch ;
    },

    /**
     * Given the slot in slotMatch[0], fill the information of args value (if exists) in slotMatch[4].
     * The args is the string value "args" in "{!1:fmt(args)}" and "{!foo:fmt(args)}"
     *
     * @param {Array} slotMatch -
     * @param {Number} startIndex - Inclusive start index to search for the args if exists
     *
     * @returns {Array/Boolean} slotMatch with slotMatch[4] filled, or false if there is no args string.
     * @private
     */
    fillArgs: function(slotMatch, parseIndex) {
        var endIndex = parseIndex ;

        if (parseIndex >= slotMatch[0].length)
            return false ;

        while( slotMatch[0][endIndex] !== ')' && slotMatch[0][endIndex] !== '}' ) {
            endIndex++ ;
        }
        slotMatch[4] = slotMatch[0].substring(parseIndex, endIndex) ;
        return slotMatch ;
    },

    /**
     * Parses the template text into `buffer`, `slots` and `tokens`. This method is called
     * automatically when the template is first used.
     * @return {Ext.app.bind.Template} this
     * @private
     */
    parse: function () {
        // NOTE: The particulars of what is stored here, while private, are likely to be
        // important to Sencha Architect so changes need to be coordinated.
        var me = this,
            text = me.text,
            buffer = [],
            slots = [],
            tokens = [],
            tokenMap = {},
            last = 0,
            tokenRe = me.tokenRe,
            pos = 0,
            fmt, i, length, match, s, slot, token;

        // Remove the initters so that we don't get called here again.
        for (i in me._initters) {
            delete me[i];
        }

        me.buffer = buffer;

        me.slots = slots;

        me.tokens = tokens;

        // text = 'Hello {foo:this.fmt(2,4)} World {bar} - {1}'
        while ((match = me.getMatch(text, last))) {
            //   0                      1          2         3           4         index
            // [ '{foo:this.fmt(2,4)}', undefined, 'foo',    'this.fmt', '2,4']        6
            // [ '{bar}',               undefined, 'bar',     undefined,  undefined]  32
            // [ '{1}',                 '1',       undefined, undefined,  undefined]  40
            length = match.index - last;
            if (length) {
                buffer[pos++] = Ext.String.unescapeBind(text.substring(last, last + length));
                last += length;
            }
            last += (s = match[0]).length;

            slot = {
                fmt: (fmt = match[3] || null),
                index: match[1] ? parseInt(match[1], 10) : null,
                not: s.charAt(1) === '!',
                token: match[2] || null
            };

            token = slot.token || String(slot.index);
            if (token in tokenMap) {
                slot.pos = tokenMap[token];
            } else {
                tokenMap[token] = slot.pos = tokens.length;
                tokens.push(token);
            }

            if (fmt) {
                if (fmt.substring(0,5) === 'this.') {
                    slot.fmt = fmt.substring(5);
                } else {
                    //<debug>
                    if (!(fmt in Ext.util.Format)) {
                        Ext.raise('Invalid format specified: "' + fmt + '"');
                    }
                    //</debug>
                    slot.scope = Ext.util.Format;
                }

                me.parseArgs(match[4], slot);
            }

            slots[pos++] = slot;
        }

        if (last < text.length) {
            buffer[pos++] = Ext.String.unescapeBind(text.substring(last));
        }

        return me;
    }


}) ;