/*
 Escaped-binds package is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License 3.0 as published by
 the Free Software Foundation.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 http://www.gnu.org/licenses/gpl.html
 */
/**
 * Override regarding to split an alone bind expression by not escaped dots.
 */
Ext.define('Nishilua.app.bind.AbstractStub', {
    override: 'Ext.app.bind.AbstractStub',

    compatibility: [
        '6.0.1.250',
        '6.2.0.981'
    ],

    /**
     * Gets the 'stub' related to the descriptor (without '{}') passed in path.
     *
     * @param {String/Object} path - The bind descriptor. Can contain  a path made from
     *                               escaped tokens with {@link Overrides.String.escapeBind}.
     * @return {Ext.app.bind.AbstractStub} The `Stub` associated to the bind descriptor.
     * @private
     */
    getChild: function (path) {
        var pathArray = path ;
        if (Ext.isString(path)) {
            // split by '.' but not by escaped ones
            pathArray = Ext.String.splitByUnescapedDots(path) ;
        }

        if (pathArray && pathArray.length) {
            return this.descend(pathArray, 0);
        }

        return this;
    }

});
