/*
 Escaped-binds package is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License 3.0 as published by
 the Free Software Foundation.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 http://www.gnu.org/licenses/gpl.html
 */
/**
 * Override to improve {@link Ext.app.ViewModel} to match correctly alone escaped '{!foo\bar}' expressions.
 */
Ext.define('Nishilua.app.ViewModel', {
    override: 'Ext.app.ViewModel',

    compatibility: '6.2.0.981',

    /**
     * This expression is used to detect when a bind expression is a '{foo}' alone. At the same time, '{foo\.bar}'
     * is an expression, but '{foo:func()}' is a Template.
     *
     * Overwrites the expressionRe to accept any alone expression in between "{}" (literals),
     * to support escaped bind descriptors.
     *
     * Does a loose test: only verifies that it is an alone escaped expression "{}", but does not guarantee
     * that the escaped elements will be only `\`, `:`, `{`, `}` and `!`
     *
     * In ExtJS 6.0.x this regexp matched optionally the negation.
     * In ExtJS 6.2.x this regexp must only match expressions without negation [nor functions (but can have escaped characters)]
     *
     * @author Jacobo Lobeiras Blanco, improved thereafter
     */
    expressionRe: /^\{([^!}:\\]|\\.)([^}:\\]|\\.)+}$/i,

    set: function (path, value) {
        var me = this,
            obj, stub;

        // Force data creation
        me.getData();

        if (value === undefined && path && path.constructor === Object) {
            stub = me.getRoot();
            value = path;
        } else if (path && Ext.String.findUnescapedCharacer(path, '.') < 0) {
            obj = {};
            obj[path] = value;
            value = obj;
            stub = me.getRoot();
        } else {
            stub = me.getStub(path);
        }

        stub.set(value);
    }

});