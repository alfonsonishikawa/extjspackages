/**
 * The main application class. An instance of this class is created by app.js when it
 * calls Ext.application(). This is the ideal place to handle application launch and
 * initialization details.
 */
Ext.define('Example62.Application', {
    extend: 'Ext.app.Application',
    
    name: 'Example62',

    stores: [
        // TODO: add global / shared stores here
    ],
    
    launch: function () {
        // TODO - Launch the application

        /**
         * Example that shows nishilua-escaped-binds usage.
         *
         * Must escape the characters: '.', '\', '{', '}', '!', ':' and ' '
         *
         */
        Ext.create('Ext.window.Window', {
            title: 'Example',
            viewModel: {
                data: {
                    'mytitle': 'Ok',
                    'my.title': 'LInked data OK',
                    my: {
                        title: 'Ok3',
                        '!{ }:': 'value with a bizarre key',
                        'number.I.like': 35,
                        'array.value': ['arrayval0', 'arrayval1']
                    }
                }
            },
            bind: {
                title: 'ExtJS 6.2.0.981 - Number of "\\{my.number\\.I\\.like\\}" = #{my.number\\.I\\.like:number("0.000")}#'
            },
            width: 550,
            items: [
                {
                    xtype:'textfield',
                    fieldLabel: 'data',
                    bind:'{mytitle}'
                },
                {
                    xtype:'textfield',
                    fieldLabel: 'data2',
                    bind:'{my\\.title}'
                },
                {
                    xtype:'textfield',
                    fieldLabel: 'data3',
                    bind: '{my.title}'
                },
                {
                    xtype:'textfield',
                    fieldLabel: 'data4',
                    bind: '{my.' + Ext.String.escapeBind('!{ }:') + '}'
                },
                {
                    xtype:'textfield',
                    fieldLabel: 'linked data with field data2',
                    bind: '{my\\.title}'
                },
                {
                    xtype:'numberfield',
                    fieldLabel: 'title number',
                    bind: '{my.number\\.I\\.like}'
                },
                {
                    xtype:'textfield',
                    fieldLabel: 'Array value 0',
                    bind: '{my.array\\.value.0}'
                },
                {
                    xtype:'textfield',
                    fieldLabel: 'Array value 1',
                    bind: '{my.array\\.value.1}'
                },
                {
                    xtype: 'checkbox',
                    fieldLabel: '!(Array value 0)',
                    bind: '{!my.array\\.value.0}'
                },
                {
                    xtype: 'button',
                    text: 'show',
                    handler: function() {
                        alert(JSON.stringify(this.up().getViewModel().data)) ;
                        this.up().getViewModel().set('my.number\\.I\\.like',10) ;
                        this.up().getViewModel().set('my.array\\.value', []) ;
                    }
                }

            ]

        }).show() ;

    },

    onAppUpdate: function () {
        Ext.Msg.confirm('Application Update', 'This application has an update, reload?',
            function (choice) {
                if (choice === 'yes') {
                    window.location.reload();
                }
            }
        );
    }
});
