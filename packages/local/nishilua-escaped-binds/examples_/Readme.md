Examples
========

Moved from `examples` to `examples_` because of issues when building the packages.
To run the examples locally execute the followin command on the proper example:

    $ sencha app watch
