/*
 This file is part of Routes Before Filter ExtJS package.

 Routes Before Filter ExtJS package is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 Routes Before Filter ExtJS package is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with GoraExplorer. If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * Override to allow the route before filter on Controllers.
 * @override Ext.app.route.Router
 */
Ext.define('Nishilua.app.route.Router', {
    override: 'Ext.app.route.Router',


    /**
     * @property {Ext.app.route.Route[]} routesBeforeFilter - Connected {@link Ext.app.route.Route}
     * instances to be executed before every route action on different controllers
     */
    routesBeforeFilter: [],

    /**
     * Override that queues route before filter action of a controller (configured in controller's routeBeforeFilter)
     * before an action of a router is called.
     * 
     * @private
     * @param {String} token The token to react to.
     */
    onStateChange : function (token) {
        var me          = this,
            app         = me.application,
            routes      = me.routes,
            routesBeforeFilter = me.routesBeforeFilter,
            len         = routes.length,
            queueRoutes = me.queueRoutes,
            tokens      = token.split(me.multipleToken),
            t           = 0,
            length      = tokens.length,
            i, queue, route, args, matched;

        for (; t < length; t++) {
            token = tokens[t];
            matched = false;

            if (queueRoutes) {
                //create a queue
                queue = new Ext.app.route.Queue({
                    token : token
                });
            }

            for (i = 0; i < len; i++) {
                route = routes[i];
                args  = route.recognize(token);

                if (args) {
                    matched = true;
                    if (queueRoutes) {
                        // Insert the routesBeforeFilter before the route action
                        Ext.Array.forEach(routesBeforeFilter, function(routeBeforeFilter){
                            if (routeBeforeFilter.controller === route.controller) {
                                // The before filter method must have the signature (actionHash, resumeOrStop)
                                queue.queueAction(routeBeforeFilter, {args: [args.args.input]});
                            }
                        });
                        queue.queueAction(route, args);
                    } else {
                        route.execute(token, args);
                    }
                }
            }

            if (queueRoutes) {
                //run the queue
                queue.runQueue();
            }

            if (!matched && app) {
                app.fireEvent('unmatchedroute', token);
            }
        }
    },

    /**
     * Create the {@link Ext.app.route.Route} instance and connect to the
     * {@link Ext.app.route.Router} singleton, for a before filter that will be
     * called for every action of the controller given.
     *
     * @param {String} action The action on the controller to execute before any action called in the controller
     * @param {Ext.app.Controller} controller The controller associated with the
     * {@link Ext.app.route.Route}
     */
    connectBeforeFilter : function(action, controller) {
        var config = {
            url        : '', // mandatory but ignored
            action     : Ext.emptyFn, //nothing
            controller : controller,
            before: action, // The configured action actually acts as a before, emptying the queue on stop()
            beforeFilterAction: true // The element is a "Before filter" action
        };

        if (Ext.isObject(action)) {
            Ext.merge(config, action);
        }
        this.routesBeforeFilter.push(new Ext.app.route.Route(config));
    },

    /**
     * Clear all the recognized routes.
     */
    clear : function() {
        this.routes = [];
        this.routesBeforeFilter = [];
    }
});
