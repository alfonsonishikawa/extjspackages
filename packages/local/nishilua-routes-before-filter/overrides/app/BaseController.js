/*
 This file is part of Routes Before Filter ExtJS package.

 Routes Before Filter ExtJS package is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 Routes Before Filter ExtJS package is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with GoraExplorer. If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * Override to allow the route before filter on Controllers.
 * @override Ext.app.BaseController
 */
Ext.define('Nishilua.app.BaseController', {
    override: 'Ext.app.BaseController',

    config: {

        /**
         * @cfg {String|Function} routesBeforeFilter
         * @accessor
         *
         * Function or function name with a similar interface as a before route action callback. Will be called
         * before every matched action in the controller.
         * Only works if Router is configured with queueRoutes=true. If the controller callback executes stop(),
         * the route action queue is emptied.
         */
        routesBeforeFilter : null

    },

    /**
     * Override that handles me.routesBeforeFilter, connecting the method in the router.
     *
     * @param {Object} routes The routes to connect to the {@link Ext.app.route.Router}
     * @private
     */
    updateRoutes : function(routes) {
        if (routes) {
            var me = this,
                befores = me.getBefore() || {},
                Router = Ext.app.route.Router,
                routesBeforeFilter = me.getRoutesBeforeFilter(),
                url, config, method;

            if (Ext.isDefined(routesBeforeFilter) && routesBeforeFilter !== null) {
                Router.connectBeforeFilter(routesBeforeFilter, me) ;
            }

            for (url in routes) {
                config = routes[url];

                if (Ext.isString(config)) {
                    config = {
                        action : config
                    };
                }

                method = config.action;

                if (!config.before) {
                    config.before = befores[method];
                }
                //<debug>
                else if (befores[method]) {
                    Ext.log.warn('You have a before method configured on a route ("' + url + '") and in the before object property also in the "' +
                        me.self.getName() + '" controller. Will use the before method in the route and disregard the one in the before property.');
                }
                //</debug>

                //connect the route config to the Router
                Router.connect(url, config, me);
            }
        }
    }


});
